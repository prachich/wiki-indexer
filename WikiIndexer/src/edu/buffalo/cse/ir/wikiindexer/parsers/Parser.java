/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.parsers;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import org.xml.sax.Attributes;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import edu.buffalo.cse.ir.wikiindexer.wikipedia.WikipediaDocument;
import edu.buffalo.cse.ir.wikiindexer.wikipedia.WikipediaParser;

/**
 * @author nikhillo
 *
 */

public class Parser  {



	/* */
	private final Properties props;

	/**
	 * 
	 * @param idxConfig
	 * @param parser
	 */

	public Parser(Properties idxProps) {
		props = idxProps;

	}

	/* TODO: Implement this method */
	/**
	 * 
	 * @param filename
	 * @param docs
	 */

	WikipediaDocument doc ;
	Collection<WikipediaDocument> document;



	public void parse(String filename, final Collection<WikipediaDocument> docs) {



		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler()
			{

				int id = 0;
				String publishDate ;
				String author ;
				String title ;
				int count = 0;
				StringBuffer text = new StringBuffer();
				StringBuffer tagcontent = new StringBuffer();

				WikipediaDocument doc;

				Boolean read = false , idflag = false, publishDateflag = false, authorflag = false, nsflag = false;
				Boolean titleflag = false, textflag = false, linksflag = false, categoriesflag = false;
				private Hashtable<String, Integer> tags;
				public void startDocument() 
				{
					tags = new Hashtable<String, Integer>();
					//System.out.println(" begin parsing document.....");

				}

				public void endDocument() 
				{
					Enumeration<String> e = tags.keys();
					while (e.hasMoreElements()) {
						String tag = (String)e.nextElement();
						int count = ((Integer)tags.get(tag)).intValue();
					}
					//System.out.println(" end of parsing document.....");


				}

				public void startElement(String nameSpaceURI, String localName, String qName, Attributes atts) throws SAXException 
				{

					tagcontent = new StringBuffer();

					String key = qName;
					Object value = tags.get(key);

					if (value == null) {
						tags.put(key, new Integer(1));
					} 
					else {
						int count = ((Integer)value).intValue();
						count++;
						tags.put(key, new Integer(count));
					}

					if (qName.equalsIgnoreCase("id"))
					{
						idflag = true ;
						count++;
					}

					if (qName.equalsIgnoreCase("timestamp"))
					{
						publishDateflag = true ;
					}	

					if (qName.equalsIgnoreCase("username")|| qName.equalsIgnoreCase("ip"))
					{
						authorflag = true ;
					}

					if (qName.equalsIgnoreCase("title"))
					{
						titleflag = true ;
					}

					if (qName.equalsIgnoreCase("text"))
					{
						textflag = true ;
						//System.out.println(qName);
					}

					if (qName.equalsIgnoreCase("categories"))
					{
						categoriesflag = true ;
					}


					if ( qName.equalsIgnoreCase("page"))
					{
						read = true;

					} 

				}

				public void characters(char[] ch, int start, int length) 
				{

					tagcontent.append(ch,start,length);
					//System.out.println(tagcontent);


					if (idflag && count== 1)
					{
						String a = tagcontent.toString();
						id = Integer.parseInt(a);
						//System.out.println(id);
						idflag = false;


					} 	


					if (publishDateflag)
					{
						publishDate = tagcontent.toString();
						publishDateflag = false;
						//System.out.println(publishDate);

					}

					if (authorflag)
					{
						author = tagcontent.toString();

						authorflag = false;
						//System.out.println(author);
					}

					if (titleflag)
					{
						title = tagcontent.toString();

						titleflag = false;
						//System.out.println(title);
					}

					if (textflag)
					{
						text=tagcontent;
					}

					if (categoriesflag)
					{

						categoriesflag = false;

					}	 


				}

				public void endElement(String nameSpaceURI, String localName, String qName) 
				{	

					if (qName.equalsIgnoreCase("text"))
					{

						textflag = false ;


					}



					if ( qName.equalsIgnoreCase("page"))
					{
						try
						{

							doc = new WikipediaDocument(id, publishDate,author, title);
							WikipediaParser wp = new WikipediaParser();
							String txt = text.toString(); //converting to string 
							doc=wp.custom(doc, txt);   // final calling***********
							add(doc, docs);

							//System.out.println(doc.getSections().get(1).getText());
							//System.out.println(doc.getPublishDate());

							//WikipediaDocument.Section innerclass = doc.new Section(txt, txt);
							//System.out.println(doc);

						} 
						catch (Exception e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						count = 0;
					} 

				}

			};


			if(filename != null && !filename.isEmpty())
			{

				saxParser.parse(filename, handler);
			}


		} catch (ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Method to add the given document to the collection.
	 * PLEASE USE THIS METHOD TO POPULATE THE COLLECTION AS YOU PARSE DOCUMENTS
	 * For better performance, add the document to the collection only after
	 * you have completely populated it, i.e., parsing is complete for that document.
	 * @param doc: The WikipediaDocument to be added
	 * @param documents: The collection of WikipediaDocuments to be added to
	 */
	private synchronized void add(WikipediaDocument doc, Collection<WikipediaDocument> documents) {
		documents.add(doc);


	}
}
