package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;

@RuleClass(className = RULENAMES.STOPWORDS)
public class StopWordsRule implements TokenizerRule {
	String [] words={"a","about","above","after","again","against","all","his","her","am","an","and","any","are","as","at","be","by","com","for","from","how","in","it","of","on","or","that","the","this","to","was","what","when","where","who","will","with","is","do","not","of","I","i","their","The","other","because","which","would","been","have","might","being","An","A","such","hence","thus","In","i.e.","he","she","it","must"};
	List<String> stopwords = Arrays.asList(words);  
	
	@Override
	public void apply(TokenStream stream) throws TokenizerException {
		// TODO Auto-generated method stub
		
		if (stream != null) {
			String token; 
			while (stream.hasNext()) { 
				token = stream.next();
				if (token != null) {
					stream.previous();
					if (stopwords.contains(token))
					{
						stream.remove();
					}
					else
					{
						stream.next();
					}
				}
			}
			stream.reset();

		}
	}
}
