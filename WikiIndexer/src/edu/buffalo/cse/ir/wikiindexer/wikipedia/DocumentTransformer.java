/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.wikipedia;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;

import edu.buffalo.cse.ir.wikiindexer.indexer.INDEXFIELD;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.Tokenizer;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerFactory;

/**
 * A Callable document transformer that converts the given WikipediaDocument object
 * into an IndexableDocument object using the given Tokenizer
 * @author nikhillo
 *
 */
public class DocumentTransformer implements Callable<IndexableDocument> {
	/**
	 * Default constructor, DO NOT change
	 * @param tknizerMap: A map mapping a fully initialized tokenizer to a given field type
	 * @param doc: The WikipediaDocument to be processed
	 */
	Map<INDEXFIELD, Tokenizer> tknizerMap1;
	WikipediaDocument doc1;
	public DocumentTransformer(Map<INDEXFIELD, Tokenizer> tknizerMap, WikipediaDocument doc) {
		//TODO: Implement this method
	
		tknizerMap1 = tknizerMap;
		doc1 = doc;
		
	}
	
	/**
	 * Method to trigger the transformation
	 * @throws TokenizerException Inc ase any tokenization error occurs
	 */
	public IndexableDocument call() throws TokenizerException { 
		// TODO Implement this method
		IndexableDocument idoc = new IndexableDocument();
		
		idoc.docId = doc1.getId();
		
		
		TokenStream author = new TokenStream(doc1.getAuthor());
		Tokenizer t_author = tknizerMap1.get(INDEXFIELD.AUTHOR);
		t_author.tokenize(author);
		//System.out.println("=======" +author.getAllTokens());
		
		
		TokenStream categories = new TokenStream(doc1.getCategories().toString());
		Tokenizer t_categories = tknizerMap1.get(INDEXFIELD.CATEGORY);
		t_categories.tokenize(categories);
		//System.out.println("=======" +categories.getAllTokens());
		
		TokenStream links = new TokenStream(doc1.getLinks().toString());
		Tokenizer t_links = tknizerMap1.get(INDEXFIELD.LINK);
		t_links.tokenize(links);
		//System.out.println("=======" +links.getAllTokens());
		
		TokenStream term = new TokenStream(doc1.getSections().get(0).getText());
		for(int i=1;i<doc1.getSections().size();i++)
		{
			term.append(doc1.getSections().get(i).getText());
		}
		Tokenizer t_term = tknizerMap1.get(INDEXFIELD.TERM);
		t_term.tokenize(term);
		//System.out.println("=======" +term.getAllTokens());
		
		
	    idoc.addField(INDEXFIELD.AUTHOR, author);
	    idoc.addField(INDEXFIELD.CATEGORY, categories);
	    idoc.addField(INDEXFIELD.LINK, links);
	    idoc.addField(INDEXFIELD.TERM, term);
	   
	   // System.out.println("========= IN DOC TRANSFORMER===========");
		return idoc;
	}
	
}
