package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;

@RuleClass(className = RULENAMES.CAPITALIZATION)
public class CapitalizationRule implements TokenizerRule {

	@Override
	public void apply(TokenStream stream) throws TokenizerException {
		boolean first=true;
		// TODO Auto-generated method stub
		if (stream != null) {
			String token; 
			while (stream.hasNext()) { 
				token = stream.next();
				Pattern p1 = Pattern.compile("[a-z]+[A-Z]+");
				Matcher m1=p1.matcher(token);
				if(!m1.find())
				{
				if (token != null) {
					if(!token.equals(token.toUpperCase()) && !Character.isUpperCase(token.charAt(0)))
					{
						stream.previous();
						stream.set(token.toLowerCase());
						stream.next();
					}
					else if(first==true && !token.equals(token.toUpperCase()))
					{
						stream.previous();
						first=false;
						stream.set(token.toLowerCase());
						stream.next();
					}
				}
				}
			}
			stream.reset();
			
		}
	}

}
