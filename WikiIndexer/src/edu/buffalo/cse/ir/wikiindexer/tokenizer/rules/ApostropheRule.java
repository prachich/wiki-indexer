package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import java.util.HashMap;
import java.util.Map;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;

@RuleClass(className = RULENAMES.APOSTROPHE)
public class ApostropheRule implements TokenizerRule {
	Map<String, String> apowords = new HashMap<String,String>();
	
	public ApostropheRule ()
	{
		apowords.put("I'm","I am");
		apowords.put("isn't", "is not");
		apowords.put("don't", "do not");
		apowords.put("won't", "will not");
		apowords.put("shan't", "shall not");
		apowords.put("let's", "let us");
		apowords.put("we're", "we are");
		apowords.put("Should have", "Should've");
		apowords.put("They'd", "They would");
		apowords.put("She'll", "She shall");
		apowords.put("'em", "them");
		apowords.put("You're", "You are");
		apowords.put("She's", "She is");
		apowords.put("it's", "it is");
		apowords.put("I've", "I have");
		apowords.put("Should've","Should have");
		apowords.put("They'd","They would");
		apowords.put("She'll","She will");
		apowords.put("they’ll","they will");
		apowords.put("aren't","are not");
		apowords.put("can't","can not");
		apowords.put("couldn't","could not");
		apowords.put("hasn't","has not");
		apowords.put("who's","who is");
		apowords.put("that'll","that will");
		apowords.put("I'd","I would");
		apowords.put("where's","where is");
		apowords.put("weren't","were not");
		apowords.put("they're","they are");
		apowords.put("we've","we have");
		apowords.put("he'd","he would");
		apowords.put("she'd","she would");
		apowords.put("you'hv","you have");
		apowords.put("we'll","we will");
		apowords.put("I'll","I will");
		apowords.put("who're","who are");
		apowords.put("you'd","you would");
		apowords.put("what'll","what will");
		apowords.put("what're","what are");
		apowords.put("shan't","shall not");
		apowords.put("she'd","she would");
		apowords.put("let's","let us");
		apowords.put("they'd","they had");
		apowords.put("mustn't", "must not");	
		//apowords.put("'quote","quote");
	}
	
	@Override
	public void apply(TokenStream stream) throws TokenizerException {
		// TODO Auto-generated method stub
		if (stream != null) {
			String token; 
			while (stream.hasNext()) { 
				token = stream.next();
				if (token != null) {
					if(apowords.containsKey(token))
					{
						token=apowords.get(token);
						stream.previous();
						stream.set(token.split(" "));
						stream.next();
								
					}
				else if (token.contains("'s"))
					{
						token=token.replaceAll("'s", "");
						stream.previous();
						stream.set(token);
						stream.next();
								
					}
				
					else if(token.contains("'"))
					{
						token=token.replaceAll("'", "");
						stream.previous();
						stream.set(token);
						stream.next();
								
					}
					
				}
				
			}
			stream.reset();
		}

	}
}

