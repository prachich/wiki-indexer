/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;

/**
 * @author nikhillo An abstract class that represents a dictionary object for a
 *         given index
 */

public abstract class Dictionary implements Writeable {

	Map<String, Integer> dictionary;

	INDEXFIELD f;
	File file;

	public Dictionary(Properties props, INDEXFIELD field) {
		// TODO Implement this method
		f = field;
		dictionary = new HashMap<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.buffalo.cse.ir.wikiindexer.indexer.Writeable#writeToDisk()
	 */
	public void writeToDisk() throws IndexerException {
		// TODO Implement this method
		try {
			FileOutputStream fos = new FileOutputStream(
					"C:/Eclipse_Workspace/A_IR/Temp/dictionary.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(dictionary);
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.buffalo.cse.ir.wikiindexer.indexer.Writeable#cleanUp()
	 */
	public void cleanUp() {
		// TODO Implement this method
		dictionary = null;

	}

	/**
	 * Method to check if the given value exists in the dictionary or not Unlike
	 * the subclassed lookup methods, it only checks if the value exists and
	 * does not change the underlying data structure
	 * 
	 * @param value
	 *            : The value to be looked up
	 * @return true if found, false otherwise
	 */
	public boolean exists(String value) {
		// TODO Implement this method

		if (dictionary.containsKey(value)) {
			return true;
		} else {

			return false;
		}
	}

	/**
	 * MEthod to lookup a given string from the dictionary. The query string can
	 * be an exact match or have wild cards (* and ?) Must be implemented ONLY
	 * AS A BONUS
	 * 
	 * @param queryStr
	 *            : The query string to be searched
	 * @return A collection of ordered strings enumerating all matches if found
	 *         null if no match is found
	 */
	public Collection<String> query(String queryStr) {
		// TODO: Implement this method (FOR A BONUS)

		ArrayList<String> match = new ArrayList<String>();
		Set<String> s1 = dictionary.keySet();

		if (queryStr.contains("*")) {
			if (queryStr.endsWith("*")) // abc*
			{
				queryStr = queryStr.replaceAll("\\*", "");
				for (String str : s1) {
					if (str.startsWith(queryStr)) {
						match.add(str);
					}
				}
				if (match != null)
					return match;
				else
					return null;
			}

			else if (queryStr.startsWith("*")) // *abc
			{
				queryStr = queryStr.replaceAll("\\*", "");
				for (String str : s1) {
					if (str.endsWith(queryStr)) {
						match.add(str);
					}
				}
				if (match != null)
					return match;
				else
					return null;
			} else {
				String[] st = queryStr.split("\\*"); // abc*abc
				String st0 = st[0].replaceAll("\\*", "");
				String st1 = st[1].replaceAll("\\*", "");
				for (String str : s1) {
					if (str.startsWith(st0) && str.endsWith(st1)) {
						match.add(str);
					}
				}
				if (match != null)
					return match;
				else
					return null;

			}

		} else

			for (String str : s1) {
				if (str.equals(queryStr)) {
					match.add(str);
				}
			}
		if (match.size() > 0)
			return match;

		else
			return null;

	}

	/**
	 * Method to get the total number of terms in the dictionary
	 * 
	 * @return The size of the dictionary
	 */
	public int getTotalTerms() {
		// TODO: Implement this method
		if (dictionary != null) {
			return dictionary.size();
		} else {
			return -1;
		}

	}
}
