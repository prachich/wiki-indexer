package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;

@RuleClass(className = RULENAMES.NUMBERS)
public class NumberRule implements TokenizerRule {

	@Override
	public void apply(TokenStream stream) throws TokenizerException {
		// TODO Auto-generated method stub
		if (stream != null) {
			String token; 
			while (stream.hasNext()) { 
				token = stream.next();
				if (token != null) {
					token=token.replaceAll("(\\s+\\d+),(\\d+)","");
					token=token.replaceAll("(\\d.*?\\.\\d.*?)(%)","$2");
					token=token.replaceAll("(\\d+)(\\/)(\\d+)", "$2");
					token=token.replaceAll("\\s+\\d+","");
					stream.previous();
					stream.set(token);
					token=stream.next();
					
					Pattern p=Pattern.compile("\\d+");
					Matcher m=p.matcher(token);
					if(m.find())
					{
						stream.previous();
						stream.remove();
					}
					
				}
			}
			stream.reset();
		}
	}
}
