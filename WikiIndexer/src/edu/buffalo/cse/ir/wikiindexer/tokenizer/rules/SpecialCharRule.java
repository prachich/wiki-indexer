package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;

@RuleClass(className = RULENAMES.SPECIALCHARS)
public class SpecialCharRule implements TokenizerRule {

	@Override
	public void apply(TokenStream stream) throws TokenizerException {
		// TODO Auto-generated method stub
		
		if (stream != null) {
			String token;
			String [] s;
			
			while (stream.hasNext()) { 
				token = stream.next();
				if (token != null) 
				{
					if(token.equals("__/\\__"))
					{
						stream.previous();
						stream.remove();
					}
					else
						{
						if((token.equals("-")||token.equals(".")))
						{
							token=token.replaceAll("[.]", " ");
							if(token.equals(" "))
							{
								stream.previous();
								stream.remove();
							}
						}
						
					else
					{		
					Pattern p=Pattern.compile("[^a-zA-Z0-9.-]");
					Matcher m=p.matcher(token);
					if(m.find())
					{
						token=token.replaceAll("[^a-zA-Z0-9.-]", " ");
					}
					if(token.equals(" "))
					{
						stream.previous();
						stream.remove();
					}
					
					else
					{
						token=token.trim();
						s=token.split(" ");
						stream.previous();
						stream.set(s);
						stream.next();
					}
					}
					}
				}
			}
			stream.reset();
			
			while (stream.hasNext()){
				token = stream.next();
				if (token != null) 
				{
					if(token.equals(""))
					{
						stream.previous();
						stream.remove();
					}
						
				}
				
			}
			
		} 		
	}
}

	



	
	
		