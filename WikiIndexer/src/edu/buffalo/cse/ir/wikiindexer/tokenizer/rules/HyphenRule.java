package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;




import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;


@RuleClass(className = RULENAMES.HYPHEN)
public class HyphenRule implements TokenizerRule {

	public void apply(TokenStream stream) throws TokenizerException {
		// TODO Auto-generated method stub
		if (stream != null) {
			String token; 
			while (stream.hasNext()) { 
				token = stream.next();
				if (token != null) 
				{
					Pattern p1=Pattern.compile("\\s+-+\\s+");
					Matcher m1=p1.matcher(token);
					if(m1.find())
						{
						stream.previous();
						stream.remove();
						}
					
					else
						{
						token=token.replaceAll("(^-+)|(-+$)", "");
						
						 	if(Character.isDigit(token.charAt(token.length()-1)))
						 		{
						 			Pattern p2=Pattern.compile("[a-zA-Z]+-[a-zA-Z]+");
						 			Matcher m2=p2.matcher(token);
						 			if(m2.find())
									{
								
									}
						 		}
							
						 	else
								{
						 			Pattern p3=Pattern.compile("[a-zA-Z]+-[a-zA-Z]+");
						 			Matcher m3=p3.matcher(token);
						 			if(m3.find())
						 				{
										token=token.replaceAll("\\-"," ");
										
						 				}
								}
						 	stream.previous();
							stream.set(token);
							stream.next();
						 	
						 }
					 
					}
				}
			stream.reset();
		}
	}
}



