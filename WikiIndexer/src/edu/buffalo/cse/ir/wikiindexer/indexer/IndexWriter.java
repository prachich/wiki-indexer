/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author nikhillo This class is used to write an index to the disk
 * 
 */
public class IndexWriter implements Writeable {

	Map<String, Integer> inner;
	Map<Integer, Integer> innerLink;

	Map<String, Map<String, Integer>> indexer;
	Map<Integer, Map<Integer, Integer>> indexerLink; // Forward index

	INDEXFIELD key;
	INDEXFIELD value;
	String fileKey;
	File file;

	int partitionNum = 0;

	FileOutputStream fos = null;
	ObjectOutputStream oos = null;

	/**
	 * Constructor that assumes the underlying index is inverted Every index
	 * (inverted or forward), has a key field and the value field The key field
	 * is the field on which the postings are aggregated The value field is the
	 * field whose postings we are accumulating For term index for example: Key:
	 * Term (or term id) - referenced by TERM INDEXFIELD Value: Document (or
	 * document id) - referenced by LINK INDEXFIELD
	 * 
	 * @param props
	 *            : The Properties file
	 * @param keyField
	 *            : The index field that is the key for this index
	 * @param valueField
	 *            : The index field that is the value for this index
	 */
	public IndexWriter(Properties props, INDEXFIELD keyField,
			INDEXFIELD valueField) {
		this(props, keyField, valueField, false);
	}

	/**
	 * Overloaded constructor that allows specifying the index type as inverted
	 * or forward Every index (inverted or forward), has a key field and the
	 * value field The key field is the field on which the postings are
	 * aggregated The value field is the field whose postings we are
	 * accumulating For term index for example: Key: Term (or term id) -
	 * referenced by TERM INDEXFIELD Value: Document (or document id) -
	 * referenced by LINK INDEXFIELD
	 * 
	 * @param props
	 *            : The Properties file
	 * @param keyField
	 *            : The index field that is the key for this index
	 * @param valueField
	 *            : The index field that is the value for this index
	 * @param isForward
	 *            : true if the index is a forward index, false if inverted
	 */
	public IndexWriter(Properties props, INDEXFIELD keyField,
			INDEXFIELD valueField, boolean isForward) {
		// TODO: Implement this method
		fileKey = keyField.name();

		key = keyField;
		value = valueField;

		if (keyField.name().equalsIgnoreCase(INDEXFIELD.TERM.name())) {
			indexer = new HashMap<String, Map<String, Integer>>();
			//inner = new HashMap<String, Integer>();
			// System.out.println("Map formed term");
		}

		if (keyField.name().equalsIgnoreCase(INDEXFIELD.AUTHOR.name())) {
			indexer = new HashMap<String, Map<String, Integer>>();
			//inner = new HashMap<String, Integer>();
			// System.out.println("Map formed author");
		}

		if (keyField.name().equalsIgnoreCase(INDEXFIELD.CATEGORY.name())) {
			indexer = new HashMap<String, Map<String, Integer>>();
			//inner = new HashMap<String, Integer>();
			// System.out.println("Map formed category");
		}

		if (keyField.name().equalsIgnoreCase(INDEXFIELD.LINK.name())) {
			indexerLink = new HashMap<Integer, Map<Integer, Integer>>();
			//innerLink = new HashMap<Integer, Integer>();
			// System.out.println("Map formed link");
		}

	}

	/**
	 * Method to make the writer self aware of the current partition it is
	 * handling Applicable only for distributed indexes.
	 * 
	 * @param pnum
	 *            : The partition number
	 */
	public void setPartitionNumber(int pnum) {
		// TODO: Optionally implement this method
		partitionNum = pnum;
	}

	/**
	 * Method to add a given key - value mapping to the index
	 * 
	 * @param keyId
	 *            : The id for the key field, pre-converted
	 * @param valueId
	 *            : The id for the value field, pre-converted
	 * @param numOccurances
	 *            : Number of times the value field is referenced by the key
	 *            field. Ignore if a forward index
	 * @throws IndexerException
	 *             : If any exception occurs while indexing
	 */
	public void addToIndex(int keyId, int valueId, int numOccurances)
			throws IndexerException {
		// TODO: Implement this method

		// System.out.println("1++++++++++"+ fileKey +"+++++++++++++++++" +
		// keyId + "+++++++++++++++++" +valueId+ "+++++++++++++++++"
		// +numOccurances);

		if (indexerLink.containsKey(keyId)) {
			//innerLink = null;
			innerLink = new HashMap<Integer, Integer>();
			innerLink = indexerLink.get(keyId);
			innerLink.put(valueId, numOccurances); // adds to inner map
			indexerLink.put(keyId, innerLink); // updated inner map added to
												// outer map
		} else {
			//innerLink = null;
			innerLink = new HashMap<Integer, Integer>();
			innerLink.put(valueId, numOccurances);
			indexerLink.put(keyId, innerLink);
		}

	}

	/**
	 * Method to add a given key - value mapping to the index
	 * 
	 * @param keyId
	 *            : The id for the key field, pre-converted
	 * @param value
	 *            : The value for the value field
	 * @param numOccurances
	 *            : Number of times the value field is referenced by the key
	 *            field. Ignore if a forward index
	 * @throws IndexerException
	 *             : If any exception occurs while indexing
	 */
	public void addToIndex(int keyId, String value, int numOccurances)
			throws IndexerException {
		// TODO: Implement this method
		/*
		 * System.out.println("2++++++++++"+ fileKey +"+++++++++++++++++" +
		 * keyId + "+++++++++++++++++" +value+ "+++++++++++++++++"
		 * +numOccurances);
		 * 
		 * if (indexer.containsKey(keyId)) { inner = null; inner =
		 * indexer.get(keyId); inner.put(value, numOccurances);// adds to inner
		 * map indexer.put(Integer.toString(keyId), inner);// updated inner map
		 * added to outer map } else { inner = null; inner.put(value,
		 * numOccurances); indexer.put(Integer.toString(keyId), inner); }
		 */
	}

	/**
	 * Method to add a given key - value mapping to the index
	 * 
	 * @param key
	 *            : The key for the key field
	 * @param valueId
	 *            : The id for the value field, pre-converted
	 * @param numOccurances
	 *            : Number of times the value field is referenced by the key
	 *            field. Ignore if a forward index
	 * @throws IndexerException
	 *             : If any exception occurs while indexing
	 */
	public void addToIndex(String key, int valueId, int numOccurances)
			throws IndexerException {
		// TODO: Implement this method
		// System.out.println("3++++++++++"+ fileKey +"+++++++++++++++++" + key
		// + "+++++++++++++++++" +valueId+ "+++++++++++++++++" +numOccurances);

		if (indexer.containsKey(key)) {
			//inner = null;
			inner = new HashMap<String, Integer>();
			inner = indexer.get(key);
			inner.put(Integer.toString(valueId), numOccurances);// adds to inner
																// map

			indexer.put(key, inner); // updated inner map added to outer map
		} else {
			//inner = null;
			inner = new HashMap<String, Integer>();
			inner.put(Integer.toString(valueId), numOccurances);
			indexer.put(key, inner);
		}

	}

	/**
	 * Method to add a given key - value mapping to the index
	 * 
	 * @param key
	 *            : The key for the key field
	 * @param value
	 *            : The value for the value field
	 * @param numOccurances
	 *            : Number of times the value field is referenced by the key
	 *            field. Ignore if a forward index
	 * @throws IndexerException
	 *             : If any exception occurs while indexing
	 */
	public void addToIndex(String key, String value, int numOccurances)
			throws IndexerException {
		// TODO: Implement this method
		/*
		 * System.out.println("4++++++++++"+ fileKey +"+++++++++++++++++" + key
		 * + "+++++++++++++++++" +value+ "+++++++++++++++++" +numOccurances);
		 * 
		 * if (indexer.containsKey(key)) { inner = null; inner =
		 * indexer.get(key); inner.put(value, numOccurances);// adds to inner
		 * map indexer.put(key, inner);// updated inner map added to outer map }
		 * else { inner = null; inner.put(value, numOccurances);
		 * indexer.put(key, inner); }
		 */

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.buffalo.cse.ir.wikiindexer.indexer.Writeable#writeToDisk()
	 */
	public void writeToDisk() {
		// TODO Implement this method

		try {

			// System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ inside IndexWriter  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			// fos = new FileOutputStream(fileKey + partitionNum+"C:/Eclipse_Workspace/A_IR/Temp/map.ser");
			//fos = new FileOutputStream("C:/Eclipse_Workspace/WikiIndexer/Temp/"+ fileKey + partitionNum + "map.ser");
			
			fos = new FileOutputStream( fileKey + partitionNum + "map.ser");
			oos = new ObjectOutputStream(fos);
			if (key.name() == "LINK") {
				oos.writeObject(indexerLink);
			} else {
				oos.writeObject(indexer);
			}
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.buffalo.cse.ir.wikiindexer.indexer.Writeable#cleanUp()
	 */
	public void cleanUp() {
		// TODO Implement this method
		indexer = null;
		indexerLink = null;

	}

}
