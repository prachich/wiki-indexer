/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.wikipedia;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author nikhillo
 * This class implements Wikipedia markup processing.
 * Wikipedia markup details are presented here: http://en.wikipedia.org/wiki/Help:Wiki_markup
 * It is expected that all methods marked "todo" will be implemented by students.
 * All methods are static as the class is not expected to maintain any state.
 */
public class WikipediaParser {
	static WikipediaDocument doc;
	static String text;  //for storing contents of text tag

	public WikipediaDocument custom(WikipediaDocument doc1, String text1) 
	{

		doc=doc1;

		text=text1;

		String formatedText;
		parseCategories(text);
		formatedText=text;
		String[] link=parseLinks(formatedText);
		for(int i=1;i<link.length;i++)
		{
			doc.addLink(link[i]);
		}
		formatedText=parseTextFormatting(link[0]);
		formatedText= parseListItem(formatedText);

		formatedText= parseTagFormatting(formatedText);
		formatedText=parseTemplates(formatedText);
		formatedText=parseSection(formatedText);

		//System.out.println(link[0]);      //replacement text
		//System.out.println(link[1]);    //concatenated links
		//System.out.println(formatedText);
		//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		return doc;
	} 

	/* TODO */
	/**
	 * Method to parse section titles or headings.
	 * Refer: http://en.wikipedia.org/wiki/Help:Wiki_markup#Sections
	 * @param titleStr: The string to be parsed
	 * @return The parsed string with the markup removed
	 */
	public static String parseSection(String text)
	{
		Pattern p;
		StringBuffer sb_section = new StringBuffer();
		int endind=0;
		int startind=0;
		boolean startflag = true, endflag = false, firstflag = true;
		try
		{
			p = Pattern.compile("===*(.*[^=])===*");

		}

		catch (PatternSyntaxException e)
		{
			System.err.println ("Regex syntax error: " + e.getMessage ());
			System.err.println ("Error description: " + e.getDescription ());
			System.err.println ("Error index: " + e.getIndex ());
			System.err.println ("Erroneous pattern: " + e.getPattern ());
			return "error";
		} 



		Matcher m = p.matcher(text.toString());
		String sectText = null;
		String title = "Default";

		while (m.find ())

		{
			if(firstflag)
			{
				firstflag=false;
				sb_section.append(text.substring(0 ,m.start()));   
				sectText=text.substring(0, m.start());   //first section of section text with default title
				doc.addSection(title,sectText);
			}

			if(endflag)

			{
				endind = m.start();
				startflag =true;
				endflag = false;
				sb_section.append(text.substring(startind,endind));
				doc.addSection(title, text.substring(startind,endind));
			}

			if(startflag){
				title = m.group(1);
				startind= m.end();
				startflag=false;
				endflag=true;
			} 
		}

		sb_section.append(text.substring(startind));
		String sect=sb_section.toString();
		doc.addSection(title, text.substring(startind));
		return sect;

	}


	public static String parseSectionTitle(String titleStr) {
		if(titleStr==null)
		{
			return null;
		}
		if (titleStr=="")
		{
			return "";
		}
		else
		{
			Pattern p;
			String title=null;
			try
			{
				p = Pattern.compile("(===*\\s*)(.*?)(\\s*===*)");
			}
			catch (PatternSyntaxException e)
			{
				System.err.println ("Regex syntax error: " + e.getMessage ());
				System.err.println ("Error description: " + e.getDescription ());
				System.err.println ("Error index: " + e.getIndex ());
				System.err.println ("Erroneous pattern: " + e.getPattern ());
				return "error";
			}
			Matcher m = p.matcher(titleStr.toString());

			while (m.find ())
			{
				title = m.group(2);
			}

			return title;
		} 
	} 


	/* TODO */
	/**
	 * Method to parse list items (ordered, unordered and definition lists).
	 * Refer: http://en.wikipedia.org/wiki/Help:Wiki_markup#Lists
	 * @param itemText: The string to be parsed
	 * @return The parsed string with markup removed
	 */
	public static String parseListItem(String itemText) {

		String ItemText = itemText;
		if(ItemText==null)
		{
			return null;
		}
		else
		{
			String result=ItemText.replaceAll("(\\*\\s?)|(#\\s?)|^(;\\s?)|^(:\\s?)","");
			return result; 
		}
	}

	/* TODO */
	/**
	 * Method to parse text formatting: bold and italics.
	 * Refer: http://en.wikipedia.org/wiki/Help:Wiki_markup#Text_formatting first point
	 * @param text: The text to be parsed
	 * @return The parsed text with the markup removed
	 */
	public static String parseTextFormatting(String text) {

		String s1 = text;
		if(s1==null)
		{
			return null;
		}
		else
		{
			s1=s1.replaceAll("(')|('')|(''')|(''''')|(\")", "");
			String s2= s1;
			return s2;
		}

	}


	/* TODO */
	/**
	 * Method to parse *any* HTML style tags like: <xyz ...> </xyz>
	 * For most cases, simply removing the tags should work.
	 * @param text: The text to be parsed
	 * @return The parsed text with the markup removed.
	 */


	public static String parseTagFormatting(String text) {
		String formattedText = text;
		if((text==null))
		{
			return null;

		}
		if(text=="")
		{
			return "";
		}
		else
		{
			formattedText=formattedText.replaceAll("(<\\w*>)\\s*|(<\\w*/>)\\s*|(\\s*</\\w*>)\\s*|(</\\w*>)\\s*|","");
			formattedText=formattedText.replaceAll("(<.*>)\\s?","");
			formattedText=formattedText.replaceAll("(&lt;.*?&gt;)","");
			return formattedText;
		}

	}  

	/* TODO */
	/**
	 * Method to parse wikipedia templates. These are *any* {{xyz}} tags
	 * For most cases, simply removing the tags should work.
	 * @param text: The text to be parsed
	 * @return The parsed text with the markup removed
	 */

	public static String parseTemplates(String text) {
		String out="";
		if(text==null)
		{
			return null;
		}
		else
		{
			while(text.contains("{{") && text.contains("}}"))
			{
				String sub="";
				int e = text.indexOf("}}");
				if(e!=-1)
				{
					sub = text.subSequence(0, e).toString();
					int s = sub.lastIndexOf("{{");
					if(s!=-1)
						sub=sub.substring(0, s);
					if(sub.contains("{{")==false)
					{
						out+=sub;
						text=text.substring(e+2);
					}
					else
						text=sub+text.substring(e+2);
					sub="";
				}
			}
			out+=text;
		}
		return out;
	}

	/* TODO */
	/**
	 * Method to parse links and URLs.
	 * Refer: http://en.wikipedia.org/wiki/Help:Wiki_markup#Links_and_URLs
	 * @param text: The text to be parsed
	 * @return An array containing two elements as follows - 
	 *  The 0th element is the parsed text as visible to the user on the page
	 *  The 1st element is the link url
	 */

	public static String[] parseLinks(String temp) {
		String link="";
		String visiblelink="";
		ArrayList<String> LinkString = new ArrayList<>();
		LinkString.add("");
		if(temp==null | temp=="")
		{
			LinkString.add("");
			String [] parsedLinks = new String[LinkString.size()];
			parsedLinks=LinkString.toArray(parsedLinks);
			return parsedLinks;
		}

		temp=temp.replaceAll("<nowiki />","");

		//......................External Links........................
		Pattern catPattern = Pattern.compile("\\[(http[s]?://)(.*)\\]");
		Matcher matcher = catPattern.matcher(temp);
		while(matcher.find())
		{
			String matchstr = matcher.group(2);
			if(matchstr.contains(" "))

			{
				visiblelink = matchstr.substring(matchstr.indexOf(" ")+1,matchstr.length());
				link = "";
			}
			else
			{
				visiblelink = "";
				link = "";

			}
			temp=temp.replace(matcher.group(0), visiblelink);
			LinkString.add(link);

			//System.out.println(">>"+visiblelink+"**"+link+"<<");

		}

		//......................Normal Links........................    

		catPattern = Pattern.compile("\\[\\[(.*?)\\]\\]");
		matcher = catPattern.matcher(temp);
		while(matcher.find()) 
		{
			link="";
			visiblelink="";
			String matchstr = matcher.group(1);

			//System.out.println(">>>"+matchstr);
			if(matchstr.contains("|"))
			{
				if(matchstr.lastIndexOf("|") < matchstr.length()-1)
					visiblelink = matchstr.substring( matchstr.lastIndexOf("|")+1, matchstr.length() );
				link = matchstr.substring( 0,matchstr.lastIndexOf("|") );

			}

			else
			{
				link=matchstr;

			}

			//System.out.println("**"+visiblelink+"**"+link+"");
			if(visiblelink=="")
			{
				String tempv=link;
				if(tempv.contains(",")) // if "comma"
				{                          
					visiblelink = tempv.substring( 0,matchstr.indexOf(",") );
					//System.out.println("*comma");
				}
				else if(tempv.contains("(")==true)   // if "(\\w)"
				{                       
					int ei = matchstr.indexOf("(");
					if(ei>0)
						visiblelink = tempv.substring( 0,ei-1 );
					ei=matchstr.lastIndexOf(")");
					if(ei<matchstr.length())
						visiblelink =visiblelink + tempv.substring(ei+1);
					//System.out.println("*++**"+visiblelink+"**"+link+"");
				}

				else

					visiblelink = tempv;
				//System.out.println("****-****"+visiblelink+"**"+link+"");
				if(visiblelink.contains("Wikipedia:"))
				{
					if(visiblelink.contains("#")==false)
						visiblelink = visiblelink.replace("Wikipedia:","");
				}

				if(visiblelink.matches("(.*)[:](.*)[:](.*)"))
				{
					if(visiblelink.contains("Category:")==false)
					{
						visiblelink=visiblelink.replaceFirst("(.*?)[:]", "");
					}
				}

				if(visiblelink.contains("Category:"))
				{
					if(visiblelink.contains(":Category:")==true)
						visiblelink = visiblelink.replaceFirst(":","");
					else
						visiblelink = visiblelink.replace("Category:","");
				}

				if(visiblelink.contains("File:"))
					visiblelink = "";
			}
			if(link.contains(":") == false) {
				if(link.contains(" "))

					link=link.replace(" ", "_");

				link=link.substring(0, 1).toUpperCase() + link.substring(1);

			}

			else{

				link="";       //outside namespace not interested

			}



			temp =temp.replace(matcher.group(0), visiblelink);

			LinkString.add(link);   //storing Link name

		}

		LinkString.set(0, temp);

		String [] parsedLinks = new String[LinkString.size()];

		parsedLinks=LinkString.toArray(parsedLinks);

		LinkString.clear();

		return parsedLinks;
	}

	public static void parseCategories(String text)
	{
		//------------Extract the categories code starts------------------
		String text1 = text;

		String pattern = "(\\[\\[Category:)(.*)\\]\\]";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(text1);    

		while (m.find())
		{

			String str2 = m.group(2);
			doc.addCategory(str2);

		}   

		//------------Extract the categories code ends------------------
	}





}