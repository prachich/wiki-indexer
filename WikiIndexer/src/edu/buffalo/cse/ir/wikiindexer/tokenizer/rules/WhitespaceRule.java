package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;

@RuleClass(className = RULENAMES.WHITESPACE)
public class WhitespaceRule implements TokenizerRule{

	@Override
	public void apply(TokenStream stream) throws TokenizerException {
		// TODO Auto-generated method stub
		if (stream != null) {
			String token;
			while (stream.hasNext()) { 
				token = stream.next();
				if (token != null) {
					token=token.replaceAll("(\\s+)", " ");
					token=token.replaceAll("^\\s", "");
					stream.previous();
					stream.set(token.split(" "));
					stream.next();	
				}

			}
			stream.reset();
		}
	}
}
