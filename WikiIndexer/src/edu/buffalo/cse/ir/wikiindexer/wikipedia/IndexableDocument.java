/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.wikipedia;

import java.util.HashMap;
import java.util.Map;

import edu.buffalo.cse.ir.wikiindexer.indexer.INDEXFIELD;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.Tokenizer;

/**
 * A simple map based token view of the transformed document
 * @author nikhillo
 *
 */
public class IndexableDocument {
	/**
	 * Default constructor
	 */
	Map<INDEXFIELD, TokenStream> finalIndexableMap ;
	int docId;
	
	public IndexableDocument() {
		//TODO: Init state as needed
		finalIndexableMap= new HashMap<>();
		
	}
	
	/**
	 * MEthod to add a field and stream to the map
	 * If the field already exists in the map, the streams should be merged
	 * @param field: The field to be added
	 * @param stream: The stream to be added.
	 */
	public void addField(INDEXFIELD field, TokenStream stream) {
		//TODO: Implement this method
		if (finalIndexableMap.containsKey(field))
		{
			TokenStream stream1 = finalIndexableMap.get(field);
			stream1.merge(stream);
			finalIndexableMap.put(field,stream1);
		}
		
		else
		{
			finalIndexableMap.put(field, stream);
		}
	}
	
	/**
	 * Method to return the stream for a given field
	 * @param key: The field for which the stream is requested
	 * @return The underlying stream if the key exists, null otherwise
	 */
	public TokenStream getStream(INDEXFIELD key) 
	{
		//TODO: Implement this method
		if(finalIndexableMap.containsKey(key))
		{
			return finalIndexableMap.get(key);
			 
		}
		else
		{
			return null;
		}
	
	}
	
	/**
	 * Method to return a unique identifier for the given document.
	 * It is left to the student to identify what this must be
	 * But also look at how it is referenced in the indexing process
	 * @return A unique identifier for the given document
	 */
	public String getDocumentIdentifier() 
	{
		//TODO: Implement this method
		return Integer.toString(docId);
	}
	
}
