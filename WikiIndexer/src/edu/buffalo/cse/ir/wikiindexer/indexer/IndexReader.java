/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.indexer;

import java.awt.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectInputStream.GetField;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author nikhillo This class is used to introspect a given index The
 *         expectation is the class should be able to read the index and all
 *         associated dictionaries.
 */

public class IndexReader {

	static Map<String, Map<String, Integer>> indexRead;
	static Map<Integer, Map<Integer, Integer>> indexReadLink;

	INDEXFIELD f;
	FileInputStream fis = null;
	ObjectInputStream ois = null;

	/**
	 * Constructor to create an instance
	 * 
	 * @param props
	 *            : The properties file
	 * @param field
	 *            : The index field whose index is to be read
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ClassNotFoundException
	 */

	

	public IndexReader(Properties props, INDEXFIELD field)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		// TODO: Implement this method
		f = field;
		String fileKey = f.name();
		indexRead = new HashMap<String, Map<String, Integer>>();
		indexReadLink = new HashMap<Integer, Map<Integer, Integer>>();
		// System.out.println("@@@@@@ Inside Reader @@@@@@@@@");

		try {
			for (int i = 0; i < 5; i++) {
				// FileInputStream("C:/Eclipse_Workspace/WikiIndexer/Temp/"+ fileKey +i+ "map.ser");
				fis = new FileInputStream(fileKey + i + "map.ser");
				if (null != fis) 
				{
					ois = new ObjectInputStream(fis);
					indexReadLink.putAll((Map) ois.readObject());
	
				}

				fis.close();
				ois.close();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Method to get the total number of terms in the key dictionary
	 * 
	 * @return The total number of terms as above
	 */
	public int getTotalKeyTerms() {
		// TODO: Implement this method

		if (f.name().equalsIgnoreCase(INDEXFIELD.LINK.name())) {
			return indexReadLink.size();

		} else {
			return indexRead.size();
		}

	}

	/**
	 * Method to get the total number of terms in the value dictionary
	 * 
	 * @return The total number of terms as above
	 */
	public int getTotalValueTerms() {
		// TODO: Implement this method
		int count = 0;

		if (f.name().equalsIgnoreCase(INDEXFIELD.LINK.name())) {

			for (Entry<Integer, Map<Integer, Integer>> entry : indexReadLink
					.entrySet()) {
				count = count + entry.getValue().size();
			}
			return count;
		} else {
			for (Entry<String, Map<String, Integer>> entry : indexRead
					.entrySet()) {
				count = count + entry.getValue().size();
			}
			return count;
		}

	}

	/**
	 * Method to retrieve the postings list for a given dictionary term
	 * 
	 * @param key
	 *            : The dictionary term to be queried
	 * @return The postings list with the value term as the key and the number
	 *         of occurrences as value. An ordering is not expected on the map
	 */
	public Map<String, Integer> getPostings(String key) {
		// TODO: Implement this method

		if (f.name().equalsIgnoreCase(INDEXFIELD.LINK.name())) {
			Map<Integer, Integer> inner = new HashMap<>();
			Map<String, Integer> innerString = new HashMap<>();
			inner = indexReadLink.get(key);
			for (Entry<Integer, Integer> entry : inner.entrySet()) {
				innerString.put(entry.getKey().toString(), entry.getValue());
			}

			return innerString;

		} else {
			Map<String, Integer> inner = new HashMap<>();
			inner = indexRead.get(key);

			return inner;
		}

	}

	/**
	 * Method to get the top k key terms from the given index The top here
	 * refers to the largest size of postings.
	 * 
	 * @param k
	 *            : The number of postings list requested
	 * @return An ordered collection of dictionary terms that satisfy the
	 *         requirement If k is more than the total size of the index, return
	 *         the full index and don't pad the collection. Return null in case
	 *         of an error or invalid inputs
	 */
	public Collection<String> getTopK(int k) {
		// TODO: Implement this method

		if (f.name().equalsIgnoreCase(INDEXFIELD.LINK.name())) {

			ArrayList<String> arr = new ArrayList<String>();
			ArrayList<String> arrReturn = new ArrayList<String>();
			Map<Integer, Integer> map = new HashMap<Integer, Integer>();

			ArrayList<Integer> mapKeys = new ArrayList<Integer>();
			ArrayList<Integer> mapValues = new ArrayList<Integer>();

			for (Entry<Integer, Map<Integer, Integer>> entry : indexReadLink
					.entrySet()) {
				map.put(entry.getKey(), entry.getValue().size());
				mapKeys.add(entry.getKey());
				mapValues.add(entry.getValue().size());
			}

			Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();
			map1 = MapUtil.sortByValue(map);

			for (Entry<Integer, Integer> entry : map1.entrySet()) {
				arr.add(entry.getKey().toString());
			}

			if (k != 0 && indexReadLink != null && !indexReadLink.isEmpty()) {

				if (k < arr.size()) {

					for (int i = 0; i < k; i++) {
						arrReturn.add(arr.get(i));
					}

					return arrReturn;
				} else {
					return arr;
				}

			}

		} else {

			ArrayList<String> arr = new ArrayList<String>();
			ArrayList<String> arrReturn = new ArrayList<String>();
			Map<String, Integer> map = new HashMap<String, Integer>();
			Map<String, Integer> map1 = new HashMap<String, Integer>();

			ArrayList<String> mapKeys = new ArrayList<String>();
			ArrayList<String> mapValues = new ArrayList<String>();

			for (Entry<String, Map<String, Integer>> entry : indexRead
					.entrySet()) {
				map.put(entry.getKey(), entry.getValue().size());
				mapKeys.add(entry.getKey());
				mapValues.add(Integer.toString(entry.getValue().size()));
			}

			map1 = MapUtil.sortByValue(map);

			for (Entry<String, Integer> entry : map.entrySet()) {
				arr.add(entry.getKey());
			}

			if (k != 0 && indexRead != null && !indexRead.isEmpty()) {

				if (k < arr.size()) {

					for (int i = 0; i < k; i++) {
						arrReturn.add(arr.get(i));
					}

					return arrReturn;
				} else {
					return arr;
				}

			}

			else {
				return null;
			}
		}
		return null;

	}

	/**
	 * Method to execute a boolean AND query on the index
	 * 
	 * @param terms
	 *            The terms to be queried on
	 * @return An ordered map containing the results of the query The key is the
	 *         value field of the dictionary and the value is the sum of
	 *         occurrences across the different postings. The value with the
	 *         highest cumulative count should be the first entry in the map.
	 */
	public Map<String, Integer> query(String... terms) {
		// TODO: Implement this method (FOR A BONUS)
		return null;
	}

	public static class MapUtil {
		public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(
				Map<K, V> map) {
			LinkedList<Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
					map.entrySet());
			Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
				public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
					return (o1.getValue()).compareTo(o2.getValue());
				}
			});

			Map<K, V> result = new LinkedHashMap<K, V>();
			for (Map.Entry<K, V> entry : list) {
				result.put(entry.getKey(), entry.getValue());
			}
			return result;
		}
	}
}
