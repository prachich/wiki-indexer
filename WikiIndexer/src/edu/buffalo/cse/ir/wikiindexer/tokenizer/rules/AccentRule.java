package edu.buffalo.cse.ir.wikiindexer.tokenizer.rules;

import java.text.Normalizer;
import java.util.regex.Pattern;

import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenStream;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.TokenizerException;
import edu.buffalo.cse.ir.wikiindexer.tokenizer.rules.TokenizerRule.RULENAMES;


@RuleClass(className = RULENAMES.ACCENTS)
public class AccentRule implements TokenizerRule {

	@Override
	public void apply(TokenStream stream) throws TokenizerException {
		// TODO Auto-generated method stub
		if (stream != null) {
			String token; 
			while (stream.hasNext()) { 
				token = stream.next();
				if (token != null) 
				{
					String n = Normalizer.normalize(token, Normalizer.Form.NFD);
					token = Normalizer.normalize(token, Normalizer.Form.NFD); 	
					Pattern p= Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
					token=p.matcher(n).replaceAll("");
                     
					stream.previous();
					stream.set(token);
					stream.next();
				}
	
			}
			stream.reset();
		}
	}
}
