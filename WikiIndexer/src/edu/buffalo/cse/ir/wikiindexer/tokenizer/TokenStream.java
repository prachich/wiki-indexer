/**
 * 
 */
package edu.buffalo.cse.ir.wikiindexer.tokenizer;

import java.io.ObjectOutputStream.PutField;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 * This class represents a stream of tokens as the name suggests.
 * It wraps the token stream and provides utility methods to manipulate it
 * @author nikhillo
 *
 */
public class TokenStream implements Iterator<String>{

	LinkedList<String> streams;
	int iterrator=0;

	/**
	 * Default constructor
	 * @param bldr: THe stringbuilder to seed the stream
	 */
	public TokenStream(StringBuilder bldr) {
		//TODO: Implement this method

		if (null!=bldr)
		{
			streams = new LinkedList<>();
			streams.add(bldr.toString());

		}

	}


	/**
	 * Overloaded constructor
	 * @param bldr: THe stringbuilder to seed the stream
	 */
	public TokenStream(String string) {
		//TODO: Implement this method



		if (null!=string && !string.isEmpty())
		{

			streams = new LinkedList<>();
			streams.add(string);

		}


	}

	/**
	 * Method to append tokens to the stream
	 * @param tokens: The tokens to be appended
	 */
	public void append(String... tokens) 
	{
		//TODO: Implement this method
		if (tokens!=null)
		{
			for(String token:tokens)
			{
				if (token!=""&&token!=null)
				{
					streams.add(token);

				}

			}	

		}

	}

	/**
	 * Method to retrieve a map of token to count mapping
	 * 
	 * This map should contain the unique set of tokens as keys
	 * The values should be the number of occurrences of the token in the given stream
	 * @return The map as described above, no restrictions on ordering applicable
	 */
	public Map<String, Integer> getTokenMap()
	{
		//TODO: Implement this method


		Map<String, Integer>  streamMap=  new HashMap<String, Integer>();

		if (streams != null)
		{
			String str = streams.toString();
			str = str.substring(1, str.length() -1);

			String[] a= str.split(", ") ;
			if( str!=null && !str.isEmpty() && !str.equals("null") && !str.equals(""))
			{

				for(int i =0 ; i < a.length ; i++)
				{

					String token = a[i];

					if(streamMap.containsKey(token))
					{
						streamMap.put(token , streamMap.get(token)+1);
					}
					else
					{
						streamMap.put(token, 1);
					}

				}
				return streamMap;
			}

		}
		return null;
	}

	/**
	 * Method to get the underlying token stream as a collection of tokens
	 * @return A collection containing the ordered tokens as wrapped by this stream
	 * Each token must be a separate element within the collection.
	 * Operations on the returned collection should NOT affect the token stream
	 */
	public Collection<String> getAllTokens() {
		//TODO: Implement this method

		if(streams!=null)
		{
			LinkedList<String> getTokens= new LinkedList<>();

			for (String a: streams)
			{
				getTokens.add(a);
			}


			//	Collections.sort(sortedTokens);
			return getTokens;
		}
		return null;
	}

	/**
	 * Method to query for the given token within the stream
	 * @param token: The token to be queried
	 * @return: THe number of times it occurs within the stream, 0 if not found
	 */
	public int query(String token) {
		//TODO: Implement this method
		int count = 0;
		if (streams!=null)
		{
			if (streams.contains(token))
			{

				for(int i =0 ; i < (streams.size()); i++)
				{		
					if (token.equals(streams.get(i)))
					{

						count++;

					}
				}
				return count;
			}
			else
			{
				return 0;
			}
		}
		return 0;
	}

	/**
	 * Iterator method: Method to check if the stream has any more tokens
	 * @return true if a token exists to iterate over, false otherwise
	 */
	public boolean hasNext() {
		// TODO: Implement this method
		// return streams.iterator().hasNext();


		if(null!=streams)
		{ 
			if(iterrator == streams.size()) 
				return false; 
			if( null != streams.get(iterrator))
				return true; 
		} 
		return false; 
		// TODO: Implement this method //return streams.iterator().hasNext();


	}



	/**
	 * Iterator method: Method to check if the stream has any more tokens
	 * @return true if a token exists to iterate over, false otherwise
	 */
	public boolean hasPrevious() {
		//TODO: Implement this method



		if(null!=streams)
		{ 
			if (iterrator == 0) 
				return false; 
			if( null != streams.get(iterrator-1)) return true; 
		} 
		return false;







	}

	/**
	 * Iterator method: Method to get the next token from the stream
	 * Callers must call the set method to modify the token, changing the value
	 * of the token returned by this method must not alter the stream
	 * @return The next token from the stream, null if at the end
	 */
	public String next() {
		// TODO: Implement this method
		//return streams.iterator().next();


		if(null!=streams)
		{ 
			if(iterrator == streams.size())
				return null; 
			String token = streams.get(iterrator); 
			iterrator++; 
			if( null != token)
				return token; 
		} 
		return null;

	}


	/**
	 * Iterator method: Method to get the previous token from the stream
	 * Callers must call the set method to modify the token, changing the value
	 * of the token returned by this method must not alter the stream
	 * @return The next token from the stream, null if at the end
	 */
	public String previous() {
		//TODO: Implement this method

		if(iterrator==0)
		{
			return null;
		}
		iterrator--;
		return streams.get(iterrator);

	}

	/**
	 * Iterator method: Method to remove the current token from the stream
	 */
	public void remove() 
	{
		// TODO: Implement this method
		if (streams != null)
		{
			if(iterrator < streams.size())
			{
				streams.remove(iterrator);
			}
		}

	}

	/**
	 * Method to merge the current token with the previous token, assumes whitespace
	 * separator between tokens when merged. The token iterator should now point
	 * to the newly merged token (i.e. the previous one)
	 * @return true if the merge succeeded, false otherwise
	 */
	public boolean mergeWithPrevious() 
	{
		//TODO: Implement this method

		if(streams != null)
		{
			if(iterrator == 0 || streams.size() == 1) 
				return false;
			String token = streams.get(iterrator); 
			streams.remove(iterrator); 
			iterrator--;
			streams.set(iterrator, streams.get(iterrator) + " " + token); 
			return true;
		} 
		return false;


	}


	/**
	 * Method to merge the current token with the next token, assumes whitespace
	 * separator between tokens when merged. The token iterator should now point
	 * to the newly merged token (i.e. the current one)
	 * @return true if the merge succeeded, false otherwise
	 */
	public boolean mergeWithNext() {
		//TODO: Implement this method


		if(null!=streams)
		{ 
			if(iterrator == streams.size())
				return false; 
			if ((iterrator + 1) != streams.size()) 
			{ 
				String token = streams.get(iterrator + 1);
				streams.remove(iterrator + 1); 
				streams .set(iterrator, streams.get(iterrator) + " " + token); 
				return true; 
			}
		} 
		return false; 

	}

	/**
	 * Method to replace the current token with the given tokens
	 * The stream should be manipulated accordingly based upon the number of tokens set
	 * It is expected that remove will be called to delete a token instead of passing
	 * null or an empty string here.
	 * The iterator should point to the last set token, i.e, last token in the passed array.
	 * @param newValue: The array of new values with every new token as a separate element within the array
	 */
	public void set(String... newValue) {
		//TODO: Implement this method





		if (streams != null && !streams.isEmpty() && !streams.equals("null") && !streams.equals(""))
		{
			if (iterrator == streams.size()&& newValue[0]!= null && newValue[0]!="")
			{
				streams.add(iterrator, newValue[0]);
			}
			if (newValue[0]!= null && newValue[0]!="")
			{
				streams.set(iterrator,newValue[0] );

			}

			for (int i = 1 ; i < newValue.length; i++ )


				if (newValue[i] != null && newValue[i] != "" )
				{
					iterrator++;
					streams.add(iterrator, newValue[i]);

				}

		}

	}


	/**
	 * Iterator method: Method to reset the iterator to the start of the stream
	 * next must be called to get a token
	 */
	public void reset() {
		//TODO: Implement this method

		iterrator = 0;

	}

	/**
	 * Iterator method: Method to set the iterator to beyond the last token in the stream
	 * previous must be called to get a token
	 */
	public void seekEnd() {

		if(streams != null)
		{

			iterrator= streams.size();
		}

	}

	/**
	 * Method to merge this stream with another stream
	 * @param other: The stream to be merged
	 */
	public void merge(TokenStream other) {
		//TODO: Implement this method


		if(null != other)
			if(null != other.streams)
			{ 
				if(null == streams)
				{ 
					streams = new LinkedList<>(); 
				} 
				streams.addAll(other.getAllTokens());
			}




	}
}
